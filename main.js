import { exec } from 'node:child_process';
import {
    readFile,
    writeFile
} from 'node:fs/promises';

import '@lu.se/console';
import Eris from 'eris';
import { JSDOM } from 'jsdom';
import { Command, Argument } from 'commander';
import KeyEvent from 'keyevent.json' assert { type: 'json' };
import { M3uParser } from 'm3u-parser-generator';
import { parse as parseXspf } from 'xspf-js';
import outdent from 'outdent';
import AsciiTable from 'ascii-table';
import dayjs from 'dayjs';
import stringArgv from 'string-argv';

import {
    botToken,
    botWhitelist,
    adbIp,
    adbPort,
    localMediaRootPath
} from './config.js';

let
    lastMessageTimestamp,
    lastBotTimestamp,
    lastCommandTimestamp,
    lastResponseTimestamp;

const
    eris = new Eris(
        botToken,
        { intents: ['directMessages'] }
    ),
    execPromise = command => new Promise((resolve, reject) => exec(
        command,
        (
            error,
            stdout,
            stderr
        ) => {
            if(error || stderr)
                reject(error || stderr);
            else
                resolve(stdout.replace(/\r/g, '').trim());
        }
    )),
    adb = command => execPromise(`adb -s ${adbIp}:${adbPort} shell "${command.replaceAll('"', '\\"')}"`),
    getConfig = async id => {
        try {
            return JSON.parse(await readFile(`./data/${id}.json`, 'utf8'));
        }
        catch {
            return {};
        }
    },
    setConfig = async (id, config) => await writeFile(`./data/${id}.json`, JSON.stringify(config)),
    parseMediaUrl = url => {
        const
            pathname = decodeURI(decodeURI(new URL(url).pathname)),
            title = pathname
                .slice(pathname.lastIndexOf('/') + 1, pathname.lastIndexOf('.'))
                .replace(/[\s\-._()&+\[\],]/g, ' ')
                .replace(/\s+/g, ' ');
        return {
            url,
            title,
            keywords: title.toLowerCase().split(' ')
        };
    },
    getDom = async (beforeCommand, afterCommand) => new JSDOM(await adb(`${beforeCommand ? `${beforeCommand} &&` : ''} uiautomator dump > /dev/null ${afterCommand ? `&& ${afterCommand}` : ''} && cat /sdcard/window_dump.xml`), { contentType: 'text/xml' }).window.document,
    getDomElementRect = element => {
        let [
            left,
            top,
            right,
            bottom
        ] = /** @type {string[]} */ element.getAttribute('bounds').split(/[\[\],]/).filter(Boolean);
        left = parseInt(left);
        top = parseInt(top);
        right = parseInt(right);
        bottom = parseInt(bottom);
        return {
            left,
            top,
            right,
            bottom,
            width: right - left,
            height: bottom - top
        };
    },
    clickDomElement = async ({
        dom,
        element,
        selector
    }) => {
        if(!dom)
            dom = await getDom();
        if(!element)
            element = dom.querySelector(selector);
        const
            {
                left,
                width,
                top,
                height
            } = getDomElementRect(element),
            centerX = left + Math.round(width / 2),
            centerY = top + Math.round(height / 2);
        return adb(`input tap ${centerX} ${centerY}`);
    },
    handleCommand = async ({
        content,
        authorId,
        attachments,
        botTimestamp,
        timestamp
    }) => {
        const program = new Command('').allowExcessArguments(false);
        let response;
        program.exitOverride();
        program.configureOutput({
            writeOut: output => response = { output, isCodeBlock: true },
            writeErr: output => response = { output, isCodeBlock: true }
        });
        program
            .command('shell')
            .description('Execute arbitrary shell command')
            .argument('<command>', 'Arbitrary shell command')
            .action(async command => {
                response = {
                    output: await adb(command),
                    isCodeBlock: true
                }
            });
        [
            {
                name: 'home',
                alias: 'h',
                description: 'Go to home',
                keyEvent: KeyEvent.KEYCODE_HOME
            },
            {
                name: 'back',
                alias: 'b',
                description: 'Go back',
                keyEvent: KeyEvent.KEYCODE_BACK
            },
            {
                name: 'p',
                description: 'Play/pause media',
                keyEvent: KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
            },
            {
                name: 'play',
                description: 'Play media',
                keyEvent: KeyEvent.KEYCODE_MEDIA_PLAY
            },
            {
                name: 'pause',
                description: 'Pause media',
                keyEvent: KeyEvent.KEYCODE_MEDIA_PAUSE
            },
            {
                name: 'prev',
                description: 'Play previous media',
                keyEvent: KeyEvent.KEYCODE_MEDIA_PREVIOUS
            },
            {
                name: 'next',
                alias: 'n',
                description: 'Play next media',
                keyEvent: KeyEvent.KEYCODE_MEDIA_NEXT
            }
        ].forEach(({
            name,
            alias,
            description,
            keyEvent
        }) => {
            const command = program.command(name);
            if(alias) command.alias(alias);
            command.description(description);
            command.argument('[i]', 'Iteration count', _ => parseInt(_));
            command.action((i = 1) => adb(Array(i).fill(`input keyevent ${keyEvent}`).join(' && ')));
        });
        program
            .command('jump')
            .alias('j')
            .description('Jump media playback at X%')
            .argument('<x>', 'Position as percentage', _ => parseFloat(_))
            .action(async x => {
                const seekBarElement = (await getDom()).querySelector(`
                    [resource-id="com.spotify.music:id/seek_bar"],
                    [resource-id="is.xyz.mpv:id/playbackSeekbar"],
                    [resource-id="com.android.gallery3d:id/controller_timebar"],
                    [resource-id="com.liskovsoft.smarttubetv.beta:id/playback_progress"]
                `);
                if(!seekBarElement)
                    return response = { embed: { title: `❌ No seek bar found` } };
                const seekBarElementRect = getDomElementRect(seekBarElement);
                await adb(`input tap ${seekBarElementRect.left + Math.round(seekBarElementRect.width * x / 100)} ${seekBarElementRect.top + Math.round(seekBarElementRect.height / 2)}`);
            });
        program
            .command('type')
            .description('Type text')
            .argument('<text>', 'Text to type')
            .action(text => adb(`input text ${text}`));
        program
            .command('press')
            .description('Press key')
            .addArgument(
                new Argument('<key>', 'Key to press')
                    .choices(['tab', 'enter'])
            )
            .action(key => adb(`input keyevent ${{
                'tab': KeyEvent.KEYCODE_TAB,
                'enter': KeyEvent.KEYCODE_ENTER
            }[key]}`));
        program
            .command('click')
            .description('Click a button')
            .argument('<label>', 'Button label')
            .action(async label => {
                const
                    dom = await getDom(),
                    element = dom.querySelector(`[text*="${label}" i]`);
                if(!element)
                    return response = { embed: { title: `❌ Button not found` } };
                await clickDomElement({
                    dom,
                    element
                });
            });
        program
            .command('spotify')
            .description('Launch Spotify')
            .action(() => adb('monkey -p com.spotify.music 1'));
        program
            .command('mpv')
            .description('Play media on MPV')
            .argument('<url>', 'Media URL')
            .action(url => adb(`am start -a android.intent.action.VIEW -d "${url}" -t video/any is.xyz.mpv`));
        program
            .command('mpv.s')
            .description('Toggle MPV stats')
            .action(async () => {
                await adb(`input keyevent ${KeyEvent.KEYCODE_DPAD_UP} && input tap 1860 0`);
                await clickDomElement({ selector: '[resource-id="is.xyz.mpv:id/advancedBtn"]' });
                await clickDomElement({ selector: '[resource-id="is.xyz.mpv:id/statsBtn"]' });
                await adb(`input keyevent ${KeyEvent.KEYCODE_DPAD_UP} && input keyevent ${KeyEvent.KEYCODE_DPAD_UP}`);
            });
        program
            .command('playlist')
            .alias('pl')
            .description('Manage playlists & play on MPV')
            .argument('[name]', 'Playlist name')
            .argument('[media...]', 'Playlist media index/title/partial title')
            .action(async (name, media) => {
                media = media.join(' ');
                const
                    config = await getConfig(authorId) || {},
                    newPlaylistUrl = attachments[0]?.url;
                let { playlists } = config;
                if(!playlists)
                    config.playlists = playlists = [];
                if(!playlists.length)
                    return response = { embed: { title: `❌ No existing playlist` } };
                const matchingPlaylists = playlists.filter(playlist => playlist.name.toLowerCase().includes(name?.toLowerCase()));
                if(matchingPlaylists.length > 1 || !name)
                    return response = {
                        embed: {
                            title: `ℹ️ ${name ? 'Matching' : 'All'} playlists :`,
                            description: (name ? matchingPlaylists : playlists).map(playlist => `- [${playlist.name}](${playlist.url})`).join('\n')
                        }
                    };
                let [playlist] = matchingPlaylists;
                if(newPlaylistUrl){
                    if(!/\.(m3u8?|xspf)$/i.test(new URL(newPlaylistUrl).pathname))
                        return response = { embed: { title: `❌ Invalid playlist` } };
                    response = { embed: { title: `✅ Playlist updated` } };
                    if(!playlist){
                        playlists.push(playlist = { name });
                        response = { embed: { title: `✅ Playlist created` } };
                    }
                    playlist.url = newPlaylistUrl;
                    return setConfig(authorId, config);
                }
                if(!playlist)
                    return response = { embed: { title: `❌ Playlist not found` } };
                ({
                    'refreshed_urls': [{ 'refreshed': playlist.url }]
                } = await eris.requestHandler.request(
                    'POST',
                    '/attachments/refresh-urls',
                    true,
                    { 'attachment_urls': [playlist.url] }
                ));
                const { pathname: playlistPathname } = new URL(playlist.url);
                let
                    rawPlaylistData = await (await fetch(playlist.url)).text(),
                    playlistItems;
                if(/\.m3u8?$/i.test(playlistPathname)){
                    if(!rawPlaylistData.startsWith('#EXTM3U'))
                        rawPlaylistData = `#EXTM3U\n${rawPlaylistData}`;
                    playlistItems = M3uParser.parse(rawPlaylistData).medias.map(item => parseMediaUrl(item['location']));
                }
                if(/\.xspf?$/i.test(playlistPathname))
                    playlistItems = parseXspf(rawPlaylistData)['playlist']['tracks'].map(item => parseMediaUrl(item['location']));
                if(!media)
                    return response = {
                        output: outdent `
                            📀 ${playlist.name}
                            ${playlistItems.map((item, index) => `${index + 1}. \`${item.title}\``).join('\n')}
                        `
                    };
                const
                    isIndex = /^\d+$/.test(media),
                    keywords = isIndex ? undefined : media.toLowerCase().split(' '),
                    matchingPlaylistItems = isIndex
                        ? [playlistItems[parseInt(media) - 1]]
                        : playlistItems.filter(item => keywords.every(keyword => item.title.toLowerCase().includes(keyword)));
                if(matchingPlaylistItems.length === 0)
                    return response = { embed: { title: `❌ Media not found` } };
                if(matchingPlaylistItems.length > 1)
                    return response = {
                        output: outdent `
                            🔎 Multiple media found :
                            ${matchingPlaylistItems.map(item => `${playlistItems.indexOf(item) + 1}\\. \`${item.title}\``).join('\n')}
                        `
                    };
                const [matchingPlaylistItem] = matchingPlaylistItems;
                await adb(`am start -a android.intent.action.VIEW -d ${matchingPlaylistItem.url} -t video/any is.xyz.mpv`);
                return response = { embed: { title: `▶️ \`${matchingPlaylistItem.title}\` (${playlistItems.indexOf(matchingPlaylistItem) + 1}/${playlistItems.length})` } };
            });
        program
            .command('playlist.rm')
            .alias('pl.rm')
            .description('Remove playlist')
            .argument('<name>', 'Playlist name')
            .action(async name => {
                const
                    config = await getConfig(authorId) || {},
                    { playlists } = config,
                    playlistIndex = playlists?.findIndex(playlist => playlist.name.toLowerCase() === name.toLowerCase());
                if([undefined, -1].includes(playlistIndex))
                    return response = { embed: { title: `❌ No existing playlist by that name` } };
                playlists.splice(playlistIndex, 1);
                await setConfig(authorId, config);
                response = { embed: { title: `✅ Playlist removed` } };
            });
        program
            .command('local')
            .alias('l')
            .description('List & play local media')
            .argument('[media...]', 'Media title/partial title')
            .action(async title => {
                title = title.join(' ');
                const mediaList = (await adb(`find ${localMediaRootPath} -iname "*.mkv" -o -iname "*.mp4"`)).split('\n').map(path => parseMediaUrl(`file://${path}`));
                if(!title)
                    return response = {
                        output: outdent `
                            📂 Local media :
                            ${mediaList.map(item => `- \`${item.title}\``).join('\n')}
                        `
                    };
                const
                    keywords = title.toLowerCase().split(' '),
                    matchingMediaList = mediaList.filter(item => keywords.every(keyword => item.title.toLowerCase().includes(keyword)));
                if(matchingMediaList.length === 0)
                    return response = { embed: { title: `❌ Media not found` } };
                if(matchingMediaList.length > 1)
                    return response = {
                        output: outdent `
                            🔎 Multiple media found :
                            ${matchingMediaList.map(item => `- \`${item.title}\``).join('\n')}
                        `
                    };
                const [matchingMedia] = matchingMediaList;
                await adb(`am start -a android.intent.action.VIEW -n com.android.gallery3d/.app.MovieActivity -d "${matchingMedia.url}" -e MediaPlayerType 2`);
                return response = { embed: { title: `▶️ \`${matchingMedia.title}\`` } };
            });
        program
            .command('youtube')
            .alias('yt')
            .description('Play media on YouTube')
            .argument('[media...]', 'Media URL or search query')
            .action(media => {
                media = media.join(' ');
                if(!media)
                    return adb('monkey -p com.liskovsoft.smarttubetv.beta 1');
                let url;
                try {
                    url = new URL(media);
                }
                catch {
                    url = new URL('https://www.youtube.com/results');
                    url.searchParams.set('search_query', media);
                }
                return adb(`am start -a android.intent.action.VIEW -d "${url}" com.liskovsoft.smarttubetv.beta`);
            });
        program
            .command('twitch')
            .description('Launch Twitch')
            .argument('[url]', 'Twitch URL')
            .action(url => adb(url
                ? `am start -a android.intent.action.VIEW -d "${url}" com.github.andreyasadchy.xtra`
                : 'monkey -p com.github.andreyasadchy.xtra 1'
            ));
        program
            .command('kiwi')
            .description('Launch Kiwi browser')
            .argument('[url]', 'Website URL')
            .action(url => adb(url
                ? `am start -a android.intent.action.VIEW -d "${url}" -t text/plain com.kiwibrowser.browser`
                : 'monkey -p com.kiwibrowser.browser 1'
            ));
        program
            .command('nowplaying')
            .alias('np')
            .option('-f, --fast', 'Skip slow steps (shows less info)')
            .action(async ({ 'fast': isFast }) => {
                const
                    dumpsysOutput = await adb('dumpsys media_session'),
                    apps = [];
                let activeAppId = adb('dumpsys activity activities | grep mResumedActivity');
                {
                    let app;
                    for(let line of dumpsysOutput.split('\n')){
                        line = line.trim();
                        if(line.startsWith('package=')){
                            const id = line.slice(8);
                            apps.push(app = {
                                id,
                                name: {
                                    'com.spotify.music': 'Spotify',
                                    'is.xyz.mpv': 'MPV',
                                    'com.liskovsoft.smarttubetv.beta': 'YouTube',
                                    'com.github.andreyasadchy.xtra': 'Twitch'
                                }[id]
                            });
                        }
                        if(line.startsWith('state=PlaybackState {state='))
                            app.status = {
                                '0': 'Stopped',
                                '1': 'Stopped',
                                '2': 'Paused',
                                '3': 'Playing'
                            }[line.slice(27, 28)];
                        if(line.startsWith('metadata:'))
                            app.title = line.split(', description=')[1].split(/,? ?null/)[0] || undefined;
                        if(line === 'Global priority session is null')
                            break;
                    }
                }
                activeAppId = (await activeAppId).split(' u0 ')[1].split('/')[0];
                let activeApp = apps.find(app => app.id === activeAppId);
                switch(activeAppId){
                    case 'com.spotify.music': {
                        if(isFast) break;
                        const
                            document = await getDom(),
                            title = document.querySelector('[resource-id="com.spotify.music:id/track_info_view_title"]')?.getAttribute('text');
                        if(title) activeApp.title = title;
                        activeApp.subtitle = document.querySelector('[resource-id="com.spotify.music:id/track_info_view_subtitle"]')?.getAttribute('text');
                        break;
                    }
                    case 'is.xyz.mpv': {
                        if(isFast || activeApp.status !== 'Paused') break;
                        const document = await getDom(
                            `input keyevent ${KeyEvent.KEYCODE_DPAD_UP} > /dev/null`,
                            `input keyevent ${KeyEvent.KEYCODE_DPAD_DOWN} > /dev/null`
                        );
                        activeApp.timestamp = document.querySelector('[resource-id="is.xyz.mpv:id/playbackPositionTxt"]')?.getAttribute('text');
                        activeApp.duration = document.querySelector('[resource-id="is.xyz.mpv:id/playbackDurationTxt"]')?.getAttribute('text');
                        break;
                    }
                    case 'com.android.gallery3d': {
                        apps.push(activeApp = { name: 'Local player' });
                        if(isFast) break;
                        const document = await getDom();
                        activeApp.title = document.querySelector('[resource-id="com.android.gallery3d:id/movie_name"]')?.getAttribute('text');
                        activeApp.status = {
                            'Pause video': 'Playing',
                            'Play video': 'Paused'
                        }[document.querySelector('[resource-id="com.android.gallery3d:id/play_pause"]')?.getAttribute('content-desc')];
                        break;
                    }
                    case 'com.liskovsoft.smarttubetv.beta': {
                        if(isFast || activeApp.status !== 'Paused') break;
                        const document = await getDom();
                        activeApp.title = document.querySelector('[resource-id="com.liskovsoft.smarttubetv.beta:id/lb_details_description_title"]')?.getAttribute('text');
                        activeApp.subtitle = document.querySelector('[resource-id="com.liskovsoft.smarttubetv.beta:id/lb_details_description_subtitle"]')?.getAttribute('text');
                        activeApp.timestamp = document.querySelector('[resource-id="com.liskovsoft.smarttubetv.beta:id/current_time"]')?.getAttribute('text');
                        activeApp.duration = document.querySelector('[resource-id="com.liskovsoft.smarttubetv.beta:id/total_time"]')?.getAttribute('text');
                        break;
                    }
                    case 'com.github.andreyasadchy.xtra': {
                        if(isFast) break;
                        const document = await getDom();
                        activeApp.subtitle = document.querySelector('[resource-id="com.github.andreyasadchy.xtra:id/username"]')?.getAttribute('text');
                        activeApp.timestamp = document.querySelector('[resource-id="com.github.andreyasadchy.xtra:id/uptime"]')?.getAttribute('text').slice(0, -7)
                        break;
                    }
                }
                if(activeApp) activeApp.isActive = true;
                const
                    getStatusEmoji = app => ({
                        'Playing': '▶️',
                        'Paused': '⏸️',
                        'Stopped': '⏹️'
                    }[app.status] || '❔'),
                    getHeading = app => `${getStatusEmoji(app)} ${app.name} ${app.title ? `\`${app.title}\`` : ''}`,
                    getDescription = app => `${app.subtitle ? `\`${app.subtitle}\`${app.timestamp ? ' | ' : ''}` : ''}${app.timestamp ? `${app.timestamp}` : ''}${app.duration ? `/${app.duration}` : ''}`,
                    playingApp = apps.find(app => app.status === 'Playing'),
                    mainApp = playingApp || activeApp;
                response = {
                    embed: {
                        fields: [
                            ...mainApp ? [{
                                name: getHeading(mainApp),
                                value: getDescription(mainApp),
                                inline: true
                            }] : [],
                            ...apps
                                .filter(app => app !== mainApp)
                                .map(app => ({
                                    name: getHeading(app),
                                    value: getDescription(app),
                                    inline: true
                                }))
                        ]
                    }
                };
            });
        program
            .command('notifications')
            .description('Show notification history')
            .action(async () => {
                const items = [];
                let item;
                for(let line of (await adb('dumpsys notification --noredact')).split('\n')){
                    line = line.trim();
                    if(line.startsWith('NotificationRecord('))
                        items.push(item = { app: line.slice(35).split(' ')[0] });
                    if(line.startsWith('android.title='))
                        item.title = line.slice(line.indexOf('(') + 1, -1);
                    if(line.startsWith('android.text='))
                        item.text = line.slice(line.indexOf('(') + 1, -1);
                    if(line.startsWith('mCreationTimeMs='))
                        item.date = parseInt(line.slice(16));
                }
                items.sort((a, b) => b.date - a.date);
                response = {
                    output: new AsciiTable()
                        .setHeading('App', 'Time', 'Content')
                        .addRowMatrix(items
                            .sort((a, b) => b.date - a.date)
                            .map(item => [
                                item.app,
                                dayjs(item.date).format('DD/MM HH:mm'),
                                `${item.title}${item.title.length + item.text.length < 50 ? ` · ${item.text}` : ''}`
                            ])
                        ).toString(),
                    isCodeBlock: true
                };
            });
        program
            .command('time')
            .description('Measure system time')
            .action(async () => {
                await adb('echo');
                const deviceTimestamp = Date.now();
                response = {
                    embed: {
                        fields: [
                            {
                                name: 'Bot time',
                                value: `${botTimestamp - timestamp}ms`,
                                inline: true
                            },
                            {
                                name: 'Device time',
                                value: `${deviceTimestamp - botTimestamp}ms`,
                                inline: true
                            },
                            {
                                name: 'Total time',
                                value: `${deviceTimestamp - timestamp}ms`,
                                inline: true
                            },
                            ...lastMessageTimestamp ? [
                                {
                                    name: 'Last bot time',
                                    value: `${lastBotTimestamp - lastMessageTimestamp}ms`,
                                    inline: true
                                },
                                {
                                    name: 'Last command time',
                                    value: `${lastCommandTimestamp - lastBotTimestamp}ms`,
                                    inline: true
                                },
                                ...lastResponseTimestamp ? [
                                    {
                                        name: 'Last reply time',
                                        value: `${lastResponseTimestamp - lastCommandTimestamp}ms`,
                                        inline: true
                                    },
                                    {
                                        name: 'Last ping',
                                        value: `${lastResponseTimestamp - lastMessageTimestamp}ms`,
                                        inline: true
                                    }
                                ] : [
                                    {
                                        name: 'Last total time',
                                        value: `${lastCommandTimestamp - lastMessageTimestamp}ms`,
                                        inline: true
                                    }
                                ]
                            ] : []
                        ]
                    }
                };
            });
        program
            .command('reboot')
            .action(() => adb('reboot'));
        program
            .command('screenshot')
            .action(async () => {
                await execPromise(`adb -s ${adbIp}:${adbPort} exec-out screencap -p > ./data/screenshot.png`);
                response = {
                    file: {
                        file: await readFile('./data/screenshot.png'),
                        name: 'screenshot.png'
                    }
                };
            });
        try {
            await program.parseAsync(
                stringArgv(`${content[0].toLowerCase()}${content.slice(1)}`),
                { from: 'user' }
            );
        }
        catch(error){
            if(!error.code?.startsWith?.('commander.'))
                throw error;
        }
        return response;
    };

eris.on(
    'error',
    error => console.error(error)
);

eris.on(
    'messageCreate',
    async ({
        guildID: guildId,
        content,
        author: {
            id: authorId,
            bot: isAuthorBot
        },
        channel: {
            id: channelId
        },
        id,
        attachments,
        createdAt: timestamp
    }) => {
        const
            botTimestamp = Date.now(),
            _reply = ({
                embed,
                content,
                file
            }) => eris.createMessage(
                channelId,
                {
                    embed,
                    content,
                    messageReference: {
                        messageID: id
                    }
                },
                file
            ),
            reply = async (content, isCodeBlock) => {
                let response;
                const lines = content.split('\n');
                let chunk = isCodeBlock ? '```\n' : '';
                for(let lineIndex = 0; lineIndex < lines.length; lineIndex++){
                    const
                        line = lines[lineIndex] + '\n',
                        canAddLine = chunk.length + line.length + (isCodeBlock ? 3 : 0) <= 2000;
                    if(canAddLine)
                        chunk += line;
                    if(!canAddLine || lineIndex === lines.length - 1){
                        if(isCodeBlock)
                            chunk += '```';
                        response = await _reply({ content: chunk });
                        chunk = isCodeBlock ? `\`\`\`\n${line}` : line;
                    }
                }
                return response;
            };
        if(isAuthorBot) return;
        if(
            guildId
            ||
            !botWhitelist.includes(authorId)
        ) return eris.addMessageReaction(channelId, id, '❌');
        eris.addMessageReaction(channelId, id, '⏳').catch(error => console.error(error));
        try {
            const {
                output,
                isCodeBlock,
                embed,
                file
            } = await handleCommand({
                content,
                authorId,
                attachments,
                botTimestamp,
                timestamp
            }) || {};
            lastCommandTimestamp = Date.now();
            lastResponseTimestamp = (output
                ? await reply(output, isCodeBlock)
                : embed || file
                    ? _reply({ embed, file })
                    : undefined
            )?.createdAt;
            await eris.addMessageReaction(channelId, id, '✅');
            lastMessageTimestamp = timestamp;
            lastBotTimestamp = botTimestamp;
        }
        catch(error){
            console.error(error);
            await eris.addMessageReaction(channelId, id, '❌');
        }
    }
);

(async () => {
    await Promise.all([
        eris.connect(),
        new Promise(resolve => eris.once('ready', resolve))
    ]);
    console.log('Ready');
    await (async function _(){
        try {
            await execPromise(`adb connect ${adbIp}:${adbPort}`);
            await adb('echo');
            eris.editStatus('online');
        }
        catch {
            eris.editStatus('idle');
        }
        setTimeout(_, 5000);
    })();
})().catch(error => console.error(error));